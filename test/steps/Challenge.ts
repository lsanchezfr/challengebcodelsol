import { Given, Then, When } from "@cucumber/cucumber";
import { driver } from '@wdio/globals';
import { ChainablePromiseElement, Element } from "webdriverio";

class EbayPage {
  private searchBar: ChainablePromiseElement<Element>;
  private searchButton: ChainablePromiseElement<Element>;

  constructor() {
    this.searchBar = driver.$('//*[@resource-id="kw"]');
    this.searchButton = driver.$('//*[@text="Buscar"]');
  }

  async open() {
    await driver.url('https://www.ebay.com/');
  }

  async performSearch(product: string) {
    await this.searchBar.setValue(product);
    await this.searchButton.click();
    await driver.pause(10000);
  }

  async obtenerElementos(): Promise<Element[]> {
    const elementos: Element[] = [];
    let elementosAnteriores: Element[] = [];

    while (true) {
      const elementosActuales = await driver.$$(
        "//*[contains(@resource-id,'item')]"
      );

      const nuevosElementos = elementosActuales.filter(
        (elemento) => !elementos.includes(elemento)
      );

      elementos.push(...nuevosElementos);

      if (nuevosElementos.length === 0) {
        break;
      }

      elementosAnteriores = elementosActuales;

      await driver.touchAction([
        { action: "press", x: 500, y: 1500 },
        { action: "wait", ms: 1000 },
        { action: "moveTo", x: 500, y: 100 },
        "release"
      ]);

      await driver.pause(1000);
    }

    return elementos;
  }
}

const ebayPage = new EbayPage();

Given('El usuario se encuentra en la pantalla principal', async function () {
  await ebayPage.open();
});

When('Realiza la busqueda de {string} en el search', async function (product: string) {
  await ebayPage.performSearch(product);
});

Then('Visualiza la cantidad de resultados', { timeout: 600 * 1000 }, async function () {
  const totalElements = await ebayPage.obtenerElementos();
  console.log("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
  console.log(`cantidad de elementos: ${totalElements.length}`);
});